from PyQt6 import QtWidgets
from PyQt6.QtSql import QSqlDatabase
from PyQt6.QtSql import QSqlTableModel

from controller.settings import SettingsWindow
from modell.inboxModell import initinbox, insertinbox
from view.Ui_main import Ui_MainWindow


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.setupUi(self)
        self.dbconnection()
        self.inbox()
        self.actionBeallitasok.triggered.connect(self.mnubeallitasok_clicked)
        self.show()

    @staticmethod
    def mnubeallitasok_clicked():
        SettingsWindow().exec()

    @staticmethod
    def dbconnection():
        db = QSqlDatabase.addDatabase('QSQLITE')
        db.setDatabaseName('modell/main.db')

    def inbox(self):
        model = QSqlTableModel()
        self.tableView.setModel(initinbox(model))
        self.tableView.hideColumn(0)
        self.tableView.horizontalHeader().setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeMode.Stretch)
        self.tableView.horizontalHeader().setSectionResizeMode(2, QtWidgets.QHeaderView.ResizeMode.Stretch)
        self.btnInboxSave.clicked.connect(
            lambda: insertinbox(self.txtInboxNev.text(), self.txtInboxElem.toPlainText(), model))
        self.txtInboxNev.clear()
        self.txtInboxElem.clear()
        self.tableView.clicked.connect(self.on_inboxupdate)
        #  a tableview-ra kattintva a kijelölt sor adatai jelenjenek meg a form űrlapján!!!

    # def on_inboxupdate(self):
    #     index = self.tableView.selectionModel().selectedRows()
    #     row = index.row()
    #     name = self.model.data(self.model.index(row, 0))
    #     leiras = self.model.data(self.model.index(row, 1))
    #     self.txtInboxNev.setText(name)
    #     self.txtInboxElem.setText(leiras)
    #
    #     data = self.tableView.model().data(index)
    #     txt = inboxelemek(data)
    #     self.txtInboxNev.setText(txt[0])
    #     self.txtInboxElem.setText(txt[1])
