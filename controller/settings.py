from PyQt6.QtWidgets import QDialog

from view.Ui_settings import Ui_settings


class SettingsWindow(QDialog, Ui_settings):
    def __init__(self):
        super(SettingsWindow, self).__init__()
        self.setupUi(self)
