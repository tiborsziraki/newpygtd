from PyQt6.QtSql import QSqlTableModel, QSqlQuery
from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QMessageBox


def initinbox(model):
    model.setTable('inbox')
    model.setEditStrategy(QSqlTableModel.EditStrategy.OnFieldChange)
    # model.setFilter("lista=1")
    model.select()
    model.setHeaderData(1, Qt.Orientation.Horizontal, "Név")
    model.setHeaderData(2, Qt.Orientation.Horizontal, "Leírás")
    return model


def insertinbox(nev, leiras, model):
    nev = str(nev)
    leiras = str(leiras)
    if nev != "":
        query = QSqlQuery()
        query.prepare("INSERT INTO inbox (nev, leiras) VALUES (:nev, :leiras)")
        query.bindValue(":nev", nev)
        query.bindValue(":leiras", leiras)
        query.exec()
        model.select()
    else:
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Icon.Warning)
        msg.setWindowTitle("Figyelem!")
        msg.setText("A mező kitöltése kötelező!")
        msg.setStandardButtons(QMessageBox.StandardButton.Ok)
        msg.exec()


def inboxrowcount(model):
    model.setQuery("SELECT * FROM feladatok WHERE lista=1")
    elemszam = model.rowCount()
    return elemszam


def inboxelemek(i):
    query = QSqlQuery()
    query.prepare("SELECT nev, leiras FROM inbox WHERE id="+i)
    query.exec()
    query.next()
    nev = query.record().value("nev")
    leiras = query.record().value("leiras")
    return nev, leiras
