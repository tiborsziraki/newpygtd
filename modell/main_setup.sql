--
-- File generated with SQLiteStudio v3.2.1 on H ápr. 15 20:37:08 2019
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: celok
CREATE TABLE celok (
    id      INTEGER PRIMARY KEY AUTOINCREMENT,
    nev     TEXT    NOT NULL,
    leiras  TEXT,
    jovokep TEXT    NOT NULL,
    CONSTRAINT fk_celok_jovokep FOREIGN KEY (
        jovokep
    )
    REFERENCES jovokep (leiras) 
);


-- Table: elvek
CREATE TABLE elvek (
    leiras TEXT PRIMARY KEY
              NOT NULL
);


-- Table: energia
CREATE TABLE energia (
    id  INTEGER PRIMARY KEY AUTOINCREMENT,
    nev TEXT    NOT NULL
);

INSERT INTO energia (
                        id,
                        nev
                    )
                    VALUES (
                        1,
                        'Magas'
                    );

INSERT INTO energia (
                        id,
                        nev
                    )
                    VALUES (
                        2,
                        'Közepes'
                    );

INSERT INTO energia (
                        id,
                        nev
                    )
                    VALUES (
                        3,
                        'Alacsony'
                    );


-- Table: feladatok
CREATE TABLE feladatok (
    id        INTEGER PRIMARY KEY AUTOINCREMENT,
    nev       TEXT    NOT NULL,
    leiras    TEXT,
    lista     INTEGER NOT NULL,
    projekt   INTEGER,
    fokusz    INTEGER,
    kontextus INTEGER,
    ido       INTEGER,
    energia   INTEGER,
    szemely   INTEGER,
    prioritas INTEGER,
    hatarido  DATE,
    CONSTRAINT fk_feladat_energia FOREIGN KEY (
        energia
    )
    REFERENCES energia (id),
    CONSTRAINT fk_feladat_fokusz FOREIGN KEY (
        fokusz
    )
    REFERENCES fokusz (id),
    CONSTRAINT fk_feladat_ido FOREIGN KEY (
        ido
    )
    REFERENCES ido (id),
    CONSTRAINT fk_feladat_kontextus FOREIGN KEY (
        kontextus
    )
    REFERENCES kontextus (id),
    CONSTRAINT fk_feladat_lista FOREIGN KEY (
        lista
    )
    REFERENCES listak (id),
    CONSTRAINT fk_feladat_prioritas FOREIGN KEY (
        prioritas
    )
    REFERENCES prioritas (id),
    CONSTRAINT fk_feladat_projekt FOREIGN KEY (
        projekt
    )
    REFERENCES projekt,
    CONSTRAINT fk_feladat_szemely FOREIGN KEY (
        szemely
    )
    REFERENCES szemelyek (id) 
);


-- Table: fokusz
CREATE TABLE fokusz (
    id  INTEGER PRIMARY KEY AUTOINCREMENT,
    nev TEXT    NOT NULL
);


-- Table: ido
CREATE TABLE ido (
    id  INTEGER PRIMARY KEY AUTOINCREMENT,
    nev TEXT    NOT NULL
);

INSERT INTO ido (
                    id,
                    nev
                )
                VALUES (
                    1,
                    '5 perc'
                );

INSERT INTO ido (
                    id,
                    nev
                )
                VALUES (
                    2,
                    '15 perc'
                );

INSERT INTO ido (
                    id,
                    nev
                )
                VALUES (
                    3,
                    '30 perc'
                );

INSERT INTO ido (
                    id,
                    nev
                )
                VALUES (
                    4,
                    '1 óra'
                );

INSERT INTO ido (
                    id,
                    nev
                )
                VALUES (
                    5,
                    '3 óra'
                );

INSERT INTO ido (
                    id,
                    nev
                )
                VALUES (
                    6,
                    'Fél nap'
                );

INSERT INTO ido (
                    id,
                    nev
                )
                VALUES (
                    7,
                    'Egy nap'
                );

INSERT INTO ido (
                    id,
                    nev
                )
                VALUES (
                    8,
                    'Három nap'
                );

INSERT INTO ido (
                    id,
                    nev
                )
                VALUES (
                    9,
                    'Egy hét'
                );

INSERT INTO ido (
                    id,
                    nev
                )
                VALUES (
                    10,
                    'Két hét'
                );

INSERT INTO ido (
                    id,
                    nev
                )
                VALUES (
                    11,
                    '1 hónap'
                );

INSERT INTO ido (
                    id,
                    nev
                )
                VALUES (
                    12,
                    '3 hónap'
                );

INSERT INTO ido (
                    id,
                    nev
                )
                VALUES (
                    13,
                    'Fél év'
                );

INSERT INTO ido (
                    id,
                    nev
                )
                VALUES (
                    14,
                    'Egy év'
                );


-- Table: inbox
CREATE TABLE inbox (
    id     INTEGER PRIMARY KEY AUTOINCREMENT,
    nev    TEXT    NOT NULL,
    leiras TEXT
);


-- Table: jovokep
CREATE TABLE jovokep (
    leiras   TEXT PRIMARY KEY
                  NOT NULL,
    kuldetes TEXT NOT NULL,
    FOREIGN KEY (
        kuldetes
    )
    REFERENCES kuldetes (leiras) 
);


-- Table: kontextus
CREATE TABLE kontextus (
    id  INTEGER PRIMARY KEY AUTOINCREMENT,
    nev TEXT    NOT NULL
);

INSERT INTO kontextus (
                          id,
                          nev
                      )
                      VALUES (
                          1,
                          'Telefon'
                      );

INSERT INTO kontextus (
                          id,
                          nev
                      )
                      VALUES (
                          2,
                          'Számítógép'
                      );

INSERT INTO kontextus (
                          id,
                          nev
                      )
                      VALUES (
                          3,
                          'Ügyintézés'
                      );


-- Table: kuldetes
CREATE TABLE kuldetes (
    leiras TEXT PRIMARY KEY
              NOT NULL
);


-- Table: listak
CREATE TABLE listak (
    id  INTEGER PRIMARY KEY,
    nev TEXT    NOT NULL
);

INSERT INTO listak (
                       id,
                       nev
                   )
                   VALUES (
                       1,
                       'Inbox'
                   );

INSERT INTO listak (
                       id,
                       nev
                   )
                   VALUES (
                       2,
                       'Feladatok'
                   );

INSERT INTO listak (
                       id,
                       nev
                   )
                   VALUES (
                       3,
                       'Projektek'
                   );

INSERT INTO listak (
                       id,
                       nev
                   )
                   VALUES (
                       4,
                       'Naptár'
                   );

INSERT INTO listak (
                       id,
                       nev
                   )
                   VALUES (
                       5,
                       'Valamikor'
                   );

INSERT INTO listak (
                       id,
                       nev
                   )
                   VALUES (
                       6,
                       'Vár valakire'
                   );

INSERT INTO listak (
                       id,
                       nev
                   )
                   VALUES (
                       7,
                       'Kuka'
                   );

INSERT INTO listak (
                       id,
                       nev
                   )
                   VALUES (
                       8,
                       'Referencia'
                   );

INSERT INTO listak (
                       id,
                       nev
                   )
                   VALUES (
                       9,
                       'Kész'
                   );


-- Table: prioritas
CREATE TABLE prioritas (
    id  INTEGER PRIMARY KEY AUTOINCREMENT,
    nev TEXT    NOT NULL
);

INSERT INTO prioritas (
                          id,
                          nev
                      )
                      VALUES (
                          1,
                          'Sűrgős'
                      );

INSERT INTO prioritas (
                          id,
                          nev
                      )
                      VALUES (
                          2,
                          'Fontos'
                      );

INSERT INTO prioritas (
                          id,
                          nev
                      )
                      VALUES (
                          3,
                          'Közepes'
                      );

INSERT INTO prioritas (
                          id,
                          nev
                      )
                      VALUES (
                          4,
                          'Nem fontos'
                      );

INSERT INTO prioritas (
                          id,
                          nev
                      )
                      VALUES (
                          5,
                          'Nagyon ráér'
                      );


-- Table: projekt
CREATE TABLE projekt (
    id       INTEGER PRIMARY KEY AUTOINCREMENT,
    nev      TEXT    NOT NULL,
    leiras   TEXT,
    hatarido DATE,
    allapot  BOOLEAN,
    celok    INTEGER NOT NULL,
    CONSTRAINT fk_projekt_celok FOREIGN KEY (
        celok
    )
    REFERENCES celok
);


-- Table: szemelyek
CREATE TABLE szemelyek (
    id      INTEGER PRIMARY KEY AUTOINCREMENT,
    nev     TEXT    NOT NULL,
    telefon TEXT,
    email   TEXT
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
