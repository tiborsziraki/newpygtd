--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: _celok; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._celok (
    id character varying(1) DEFAULT NULL::character varying,
    nev character varying(1) DEFAULT NULL::character varying,
    leiras character varying(1) DEFAULT NULL::character varying,
    jovokep character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE public._celok OWNER TO rebasedata;

--
-- Name: _elvek; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._elvek (
    leiras character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE public._elvek OWNER TO rebasedata;

--
-- Name: _energia; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._energia (
    id smallint,
    nev character varying(8) DEFAULT NULL::character varying
);


ALTER TABLE public._energia OWNER TO rebasedata;

--
-- Name: _feladatok; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._feladatok (
    id character varying(1) DEFAULT NULL::character varying,
    nev character varying(1) DEFAULT NULL::character varying,
    leiras character varying(1) DEFAULT NULL::character varying,
    lista character varying(1) DEFAULT NULL::character varying,
    projekt character varying(1) DEFAULT NULL::character varying,
    fokusz character varying(1) DEFAULT NULL::character varying,
    kontextus character varying(1) DEFAULT NULL::character varying,
    ido character varying(1) DEFAULT NULL::character varying,
    energia character varying(1) DEFAULT NULL::character varying,
    szemely character varying(1) DEFAULT NULL::character varying,
    prioritas character varying(1) DEFAULT NULL::character varying,
    hatarido character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE public._feladatok OWNER TO rebasedata;

--
-- Name: _fokusz; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._fokusz (
    id character varying(1) DEFAULT NULL::character varying,
    nev character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE public._fokusz OWNER TO rebasedata;

--
-- Name: _ido; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._ido (
    id smallint,
    nev character varying(9) DEFAULT NULL::character varying
);


ALTER TABLE public._ido OWNER TO rebasedata;

--
-- Name: _inbox; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._inbox (
    id smallint,
    nev character varying(7) DEFAULT NULL::character varying,
    leiras character varying(16) DEFAULT NULL::character varying
);


ALTER TABLE public._inbox OWNER TO rebasedata;

--
-- Name: _jovokep; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._jovokep (
    leiras character varying(1) DEFAULT NULL::character varying,
    kuldetes character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE public._jovokep OWNER TO rebasedata;

--
-- Name: _kontextus; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._kontextus (
    id smallint,
    nev character varying(10) DEFAULT NULL::character varying
);


ALTER TABLE public._kontextus OWNER TO rebasedata;

--
-- Name: _kuldetes; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._kuldetes (
    leiras character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE public._kuldetes OWNER TO rebasedata;

--
-- Name: _listak; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._listak (
    id smallint,
    nev character varying(12) DEFAULT NULL::character varying
);


ALTER TABLE public._listak OWNER TO rebasedata;

--
-- Name: _prioritas; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._prioritas (
    id smallint,
    nev character varying(11) DEFAULT NULL::character varying
);


ALTER TABLE public._prioritas OWNER TO rebasedata;

--
-- Name: _projekt; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._projekt (
    id character varying(1) DEFAULT NULL::character varying,
    nev character varying(1) DEFAULT NULL::character varying,
    leiras character varying(1) DEFAULT NULL::character varying,
    hatarido character varying(1) DEFAULT NULL::character varying,
    allapot character varying(1) DEFAULT NULL::character varying,
    celok character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE public._projekt OWNER TO rebasedata;

--
-- Name: _sqlite_sequence; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._sqlite_sequence (
    name character varying(9) DEFAULT NULL::character varying,
    seq smallint
);


ALTER TABLE public._sqlite_sequence OWNER TO rebasedata;

--
-- Name: _sqlite_stat1; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._sqlite_stat1 (
    tbl character varying(9) DEFAULT NULL::character varying,
    idx character varying(1) DEFAULT NULL::character varying,
    stat smallint
);


ALTER TABLE public._sqlite_stat1 OWNER TO rebasedata;

--
-- Name: _sqlite_stat4; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._sqlite_stat4 (
    tbl character varying(1) DEFAULT NULL::character varying,
    idx character varying(1) DEFAULT NULL::character varying,
    neq character varying(1) DEFAULT NULL::character varying,
    nlt character varying(1) DEFAULT NULL::character varying,
    ndlt character varying(1) DEFAULT NULL::character varying,
    sample character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE public._sqlite_stat4 OWNER TO rebasedata;

--
-- Name: _szemelyek; Type: TABLE; Schema: public; Owner: rebasedata
--

CREATE TABLE public._szemelyek (
    id character varying(1) DEFAULT NULL::character varying,
    nev character varying(1) DEFAULT NULL::character varying,
    telefon character varying(1) DEFAULT NULL::character varying,
    email character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE public._szemelyek OWNER TO rebasedata;

--
-- Data for Name: _celok; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._celok (id, nev, leiras, jovokep) FROM stdin;
\.


--
-- Data for Name: _elvek; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._elvek (leiras) FROM stdin;
\.


--
-- Data for Name: _energia; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._energia (id, nev) FROM stdin;
1	Magas
2	Közepes
3	Alacsony
\.


--
-- Data for Name: _feladatok; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._feladatok (id, nev, leiras, lista, projekt, fokusz, kontextus, ido, energia, szemely, prioritas, hatarido) FROM stdin;
\.


--
-- Data for Name: _fokusz; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._fokusz (id, nev) FROM stdin;
\.


--
-- Data for Name: _ido; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._ido (id, nev) FROM stdin;
1	5 perc
2	15 perc
3	30 perc
4	1 óra
5	3 óra
6	Fél nap
7	Egy nap
8	Három nap
9	Egy hét
10	Két hét
11	1 hónap
12	3 hónap
13	Fél év
14	Egy év
\.


--
-- Data for Name: _inbox; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._inbox (id, nev, leiras) FROM stdin;
1	1. elem	1. elem tartalma
\.


--
-- Data for Name: _jovokep; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._jovokep (leiras, kuldetes) FROM stdin;
\.


--
-- Data for Name: _kontextus; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._kontextus (id, nev) FROM stdin;
1	Telefon
2	Számítógép
3	Ügyintézés
\.


--
-- Data for Name: _kuldetes; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._kuldetes (leiras) FROM stdin;
\.


--
-- Data for Name: _listak; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._listak (id, nev) FROM stdin;
1	Inbox
2	Feladatok
3	Projektek
4	Naptár
5	Valamikor
6	Vár valakire
7	Kuka
8	Referencia
9	Kész
\.


--
-- Data for Name: _prioritas; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._prioritas (id, nev) FROM stdin;
1	Sűrgős
2	Fontos
3	Közepes
4	Nem fontos
5	Nagyon ráér
\.


--
-- Data for Name: _projekt; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._projekt (id, nev, leiras, hatarido, allapot, celok) FROM stdin;
\.


--
-- Data for Name: _sqlite_sequence; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._sqlite_sequence (name, seq) FROM stdin;
inbox	1
projekt	0
feladatok	0
celok	0
energia	3
ido	14
prioritas	5
kontextus	3
\.


--
-- Data for Name: _sqlite_stat1; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._sqlite_stat1 (tbl, idx, stat) FROM stdin;
inbox		1
ido		14
energia		3
listak		9
prioritas		5
kontextus		3
\.


--
-- Data for Name: _sqlite_stat4; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._sqlite_stat4 (tbl, idx, neq, nlt, ndlt, sample) FROM stdin;
\.


--
-- Data for Name: _szemelyek; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

COPY public._szemelyek (id, nev, telefon, email) FROM stdin;
\.


--
-- PostgreSQL database dump complete
--

