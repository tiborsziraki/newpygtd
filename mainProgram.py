# _*_ coding: utf-8 _*_

import sys
from PyQt6 import QtWidgets
from controller.mainWindow import MainWindow


app = QtWidgets.QApplication(sys.argv)
mainWindow = MainWindow()
sys.exit(app.exec())
